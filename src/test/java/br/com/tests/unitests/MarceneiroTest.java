package br.com.tests.unitests;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class MarceneiroTest {
	
	@InjectMocks
	private Marceneiro marceneiro;
	
	@Mock
	private Serra serra;

	@Before
	public void setup() {
	    MockitoAnnotations.initMocks(this);
	    
	    //Mock de tres tabuas (geral para todos os testes)
	    when(serra.corta(any(Tabua.class))).thenReturn(Arrays.asList(new Tabua(), new Tabua(), new Tabua()));
	}
	
	@Test
	public void deveSerrarEmDuasTabuas() {
		//Mock de duas tabuas (exclusivo deste teste)
		when(serra.corta(any(Tabua.class))).thenReturn(Arrays.asList(new Tabua(), new Tabua()));
		
	    List<Tabua> tabuas = marceneiro.serra(new Tabua());

	    Assert.assertEquals(2, tabuas.size()); 
	}
	
	@Test
	public void deveSerrarEmTresTabuas() {
	    List<Tabua> tabuas = marceneiro.serra(new Tabua());

	    Assert.assertEquals(3, tabuas.size()); 
	}
}
