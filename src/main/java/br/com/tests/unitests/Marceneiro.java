package br.com.tests.unitests;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

public class Marceneiro {
	@Autowired
	private Serra serra;

	public Marceneiro() {}

	public List<Tabua> serra(Tabua tabua) {
		tabua.setCentimetros(10);
	    return serra.corta(tabua);
	}
}
