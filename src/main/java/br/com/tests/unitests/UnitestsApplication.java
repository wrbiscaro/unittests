package br.com.tests.unitests;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnitestsApplication {

	public static void main(String[] args) {
		SpringApplication.run(UnitestsApplication.class, args);
	}

}
