package br.com.tests.unitests;

import java.util.List;

public interface Serra {
	public List<Tabua> corta(Tabua tabua);
}
