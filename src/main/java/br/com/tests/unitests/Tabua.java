package br.com.tests.unitests;

public class Tabua {
	private int centimetros;
	
	public Tabua() {}

	public int getCentimetros() {
		return centimetros;
	}

	public void setCentimetros(int centimetros) {
		this.centimetros = centimetros;
	}
}
